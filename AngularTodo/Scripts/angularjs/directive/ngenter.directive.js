﻿ngapp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        scope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && (typeof (fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };

        element.bind("keypress", function (event) {
            if (event.which === 13) {
                //scope.$apply(function () {
                scope.safeApply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});