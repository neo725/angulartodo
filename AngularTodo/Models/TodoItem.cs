﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AngularTodo.Models
{
    [DataContract]
    public class TodoItem
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [DataMember(Name = "title")]
        public string Title { get; set; }

        public DateTime CreateDate { get; set; }

        [DefaultValue(false)]
        [DataMember(Name = "iscomplete")]
        public bool IsComplete { get; set; }
    }
}